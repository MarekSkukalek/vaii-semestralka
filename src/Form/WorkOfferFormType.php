<?php

namespace App\Form;

use App\Entity\WorkOffer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class WorkOfferFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Názov pracovnej ponuky'])
            ->add('summary', TextareaType::class ,['label' => 'Krátky popis', 'attr' => ['rows' => 2]])
            ->add('description', TextareaType::class ,['label' => 'Detailný popis', 'attr' => ['rows' => 5]])
            ->add('salary', NumberType::class, ['label' => 'Ponúkaný plat', 'html5' => true])
            ->add('dueDate', DateType::class, ['widget' => 'single_text', 'label' => 'Koniec výberového konania', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Uložiť ponuku'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WorkOffer::class,
        ]);
    }
}
