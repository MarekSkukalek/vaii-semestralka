<?php

namespace App\Form;

use App\Entity\Partner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PartnerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Meno/názov'])
            ->add('link', TextType::class, ['label' => 'Odkaz na webstránku partnera'])
            ->add('logoFile', FileType::class, [
                'label' => 'Logo',

                'mapped' => false,
                'required' => false,

                //validácia pre obrázok
                'attr' => ['accept' => "image/*"], //klient
                'constraints' => [
                    new File([    //server
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Nahrajte súbor typu obrázok (napr. JPEG)',
                    ])
                ],
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložiť'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
            'allow_extra_fields' => true,
        ]);
    }
}
