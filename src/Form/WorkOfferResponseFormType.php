<?php

namespace App\Form;

use App\Entity\WorkOfferResponse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class WorkOfferResponseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Meno a priezvisko'])
            ->add('email', EmailType::class, ['label' => 'Email', 'help' => "Formát adresa@domena.sk"])
            ->add('phone', TextType::class, ['label' => 'Telefónne číslo', 'help' => "Formát +421 alebo 09"])
            ->add('cvFile', FileType::class, [
                'label' => 'Životopis (PDF/Docx)',

                'mapped' => false,
                'required' => true,

                //validácia pre PDF/docx súbor
                'attr' => ['accept' => ".pdf,.docx"], //klient
                'constraints' => [
                    new File([
                        'mimeTypes' => [  //server
                          'application/pdf',
                          'application/x-pdf',
                          'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        ],
                        'mimeTypesMessage' => 'Nahrajte súbor typu PDF alebo Docx',
                    ])
                ],
            ])
            ->add('offerId', HiddenType::class, ['mapped' => false])
            ->add('submit', SubmitType::class, ['label' => 'Odoslať'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WorkOfferResponse::class,
            'allow_extra_fields' => true,
        ]);
    }
}
