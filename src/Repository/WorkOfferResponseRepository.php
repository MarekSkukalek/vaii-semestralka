<?php

namespace App\Repository;

use App\Entity\WorkOfferResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WorkOfferResponse>
 *
 * @method WorkOfferResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkOfferResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkOfferResponse[]    findAll()
 * @method WorkOfferResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkOfferResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkOfferResponse::class);
    }

//    /**
//     * @return WorkOfferResponse[] Returns an array of WorkOfferResponse objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?WorkOfferResponse
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
