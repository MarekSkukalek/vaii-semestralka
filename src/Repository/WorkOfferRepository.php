<?php

namespace App\Repository;

use App\Entity\WorkOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WorkOffer>
 *
 * @method WorkOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkOffer[]    findAll()
 * @method WorkOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkOffer::class);
    }

    public function findBeforeNow(): array
    {
      return $this->createQueryBuilder('w')
                ->andWhere('w.dueDate > :now OR w.dueDate is null')
                ->setParameter('now', new \DateTime('now'))
                ->orderBy('w.id', 'DESC')
                ->getQuery()
                ->getResult();
    }

//    /**
//     * @return WorkOffer[] Returns an array of WorkOffer objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?WorkOffer
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
