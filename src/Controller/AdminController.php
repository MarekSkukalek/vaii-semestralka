<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Service\FileUploader;

use App\Entity\WorkOffer;
use App\Repository\WorkOfferRepository;
use App\Form\WorkOfferFormType;

use App\Entity\User;
use App\Form\UserFormType;
use App\Repository\UserRepository;

use App\Entity\Partner;
use App\Repository\PartnerRepository;
use App\Form\PartnerFormType;
use Symfony\Component\Form\FormError;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;




#[Route('/admin', name: 'admin_')]
class AdminController extends AbstractController
{
    public function __construct(
        private SluggerInterface $slugger,
    ) {
    }


    #[Route('/logout', name: 'logout')]
    public function logout()
    {
      throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }


    #[Route('/', name: 'index')]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {

        if ($this->getUser()) {
           $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
           return $this->render('admin/dashboard.html.twig');
         }else {

           $error = $authenticationUtils->getLastAuthenticationError();

           $lastUsername = $authenticationUtils->getLastUsername();

           return $this->render('admin/index.html.twig', [
             'last_username' => $lastUsername,
             'error'         => $error,
           ]);
         }
    }


    #[Route('/work-offers', name: 'offers')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function offers(WorkOfferRepository $workOfferRepository): Response
    {
      //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

      $allOffers = $workOfferRepository->findAll();

      return $this->render('admin/offers.html.twig', [
        'allOffers' => $allOffers
      ]);
    }

    #[Route('/work-offers/add-offer', name: 'offers_add')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function addOffer(EntityManagerInterface $entityManager, Request $request): Response
    {

        $workOffer = new WorkOffer();

        $form = $this->createForm(WorkOfferFormType::class, $workOffer);

        $form->handleRequest($request);
        //ak bol odoslaný formulár, spracuj ho a presmeruj
        if ($form->isSubmitted() && $form->isValid()) {
            $workOffer = $form->getData();

            $offerSlug = strtolower($this->slugger->slug($form->get("name")->getData()));
            $workOffer->setSlug($offerSlug);

            $entityManager->persist($workOffer);
            $entityManager->flush();
            return $this->redirectToRoute('admin_offers');
        }

        return $this->render('admin/offer-form.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    #[Route('/work-offers/edit-offer/{id}', name: 'offers_edit')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function editOffer(
      EntityManagerInterface $entityManager,
      WorkOfferRepository $workOfferRepository,
      $id,
      Request $request
    ): Response
    {

        $workOffer = $workOfferRepository->find($id);

        if (!$workOffer) {
          return $this->redirectToRoute('admin_offers');
        }

        $form = $this->createForm(WorkOfferFormType::class, $workOffer);

        $form->handleRequest($request);
        //ak bol odoslaný formulár, spracuj ho a presmeruj
        if ($form->isSubmitted() && $form->isValid()) {
            $workOffer = $form->getData();

            $offerSlug = strtolower($this->slugger->slug($form->get("name")->getData()));
            $workOffer->setSlug($offerSlug);

            $entityManager->flush();
            return $this->redirectToRoute('admin_offers');
        }

        return $this->render('admin/offer-form.html.twig',[
          'form' => $form->createView(),
        ]);

    }

    #[Route('/work-offers/delete-offer/{id}', name: 'offers_delete')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function deleteOffer(EntityManagerInterface $em, WorkOfferRepository $workOfferRepository, $id): Response
    {
      $offer = $workOfferRepository->find($id);
      if ($offer) {
        $em->remove($offer);
        $em->flush();
      }
      return $this->redirectToRoute('admin_offers');
    }

    #[Route('/work-offers/show-responses/{id}', name: 'offers_show')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function showOfferResponses(
      EntityManagerInterface $entityManager,
      WorkOfferRepository $workOfferRepository,
      $id,
      Request $request
    ): Response
    {
      $offer = $workOfferRepository->find($id);
      if (!$offer) {
        //ponuka s daným id nebola nájdená
        return $this->redirectToRoute('admin_offers');
      }
      $offerResponses = $offer->getWorkOfferResponses();
      return $this->render('admin/offer-show-responses.html.twig', [
        'allOfferResponses' => $offerResponses,
        'offer' => $offer
      ]);
    }

    #[Route('/partners', name: 'partners')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function partners(PartnerRepository $partnerRepository): Response
    {
      $allPartners = $partnerRepository->findAll();

      return $this->render('admin/partners.html.twig', [
        'allPartners' => $allPartners
      ]);
    }

    #[Route('/partners/add-partner', name: 'partners_add')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function addPartner(EntityManagerInterface $entityManager, Request $request, FileUploader $fileUploader): Response
    {
        $partner = new Partner();

        $form = $this->createForm(PartnerFormType::class, $partner);

        $form->handleRequest($request);
        //ak bol odoslaný formulár, spracuj ho a presmeruj
        if ($form->isSubmitted() && $form->isValid()) {
            $partner = $form->getData();
            $logoFile = $form->get('logoFile')->getData();

            if ($logoFile) {
                $savedLogoFilename = $fileUploader->upload($logoFile);
                $partner->setLogo($savedLogoFilename);

                $entityManager->persist($partner);
                $entityManager->flush();
                return $this->redirectToRoute('admin_partners');
            } else {
              //nebol nahratý súbor
              $fileError = new FormError("Musíte nahrať logo");
              $form->get('logoFile')->addError($fileError);
            }


        }


        return $this->render('admin/partner-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/partners/edit-partner/{id}', name: 'partners_edit')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function editPartner(
      EntityManagerInterface $entityManager,
      PartnerRepository $partnerRepository,
      $id,
      Request $request,
      FileUploader $fileUploader
    ): Response
    {

        $partner = $partnerRepository->find($id);

        if (!$partner) {
            return $this->redirectToRoute('admin_partners');
        }

        $form = $this->createForm(PartnerFormType::class, $partner);

        $form->handleRequest($request);
        //ak bol odoslaný formulár, spracuj ho a presmeruj
        if ($form->isSubmitted() && $form->isValid()) {
            $partner = $form->getData();
            $logoFile = $form->get('logoFile')->getData();

            if ($logoFile) {
                $savedLogoFilename = $fileUploader->upload($logoFile);
                $partner->setLogo($savedLogoFilename);
            }

            $entityManager->flush();

            return $this->redirectToRoute('admin_partners');
        }


        return $this->render('admin/partner-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/partners/delete-partner/{id}', name: 'partners_delete')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    public function deletePartner(EntityManagerInterface $em, PartnerRepository $partnerRepository, $id): Response
    {
      $partner = $partnerRepository->find($id);
      if ($partner) {
        $em->remove($partner);
        $em->flush();
      }
      return $this->redirectToRoute('admin_partners');
    }

    #[Route('/user-management', name: 'user_management')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    #[IsGranted('ROLE_ADMIN')]
    public function user_management(UserRepository $usersRepository): Response
    {
      //$allUsers = $usersRepository->findAll();

      $allUsers = $usersRepository->findNotAdmins();

      return $this->render('admin/users.html.twig', [
        'allUsers' => $allUsers
      ]);
    }

    #[Route('/user-management/add-user', name: 'add_user')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    #[IsGranted('ROLE_ADMIN')]
    public function addUser(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher): Response
    {
      $user = new User();

      $form = $this->createForm(UserFormType::class, $user);

      $form->handleRequest($request);
      //ak bol odoslaný formulár, spracuj ho a presmeruj
      if ($form->isSubmitted() && $form->isValid()) {
          $user = $form->getData();

          $hashedPassword = $passwordHasher->hashPassword(
              $user,
              $form->get('password')->getData()
          );
          $user->setRoles(["ROLE_USER"]);
          $user->setPassword($hashedPassword);

          $entityManager->persist($user);
          $entityManager->flush();
          return $this->redirectToRoute('admin_user_management');
      }

      return $this->render('admin/user-management-form.html.twig', [
          'form' => $form->createView(),
      ]);
    }

    #[Route('/user-management/edit-user/{id}', name: 'edit_user')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    #[IsGranted('ROLE_ADMIN')]
    public function editUser(Request $request, UserRepository $usersRepository, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher, $id): Response
    {
      $user = $usersRepository->findNotAdmin($id);

      if (!$user) {
        return $this->redirectToRoute('admin_user_management');
      }

      $form = $this->createForm(UserFormType::class, $user);

      $form->handleRequest($request);
      //ak bol odoslaný formulár, spracuj ho a presmeruj
      if ($form->isSubmitted() && $form->isValid()) {
          $user = $form->getData();

          $hashedPassword = $passwordHasher->hashPassword(
              $user,
              $form->get('password')->getData()
          );
          $user->setRoles(["ROLE_USER"]);
          $user->setPassword($hashedPassword);

          $entityManager->persist($user);
          $entityManager->flush();
          return $this->redirectToRoute('admin_user_management');
      }

      return $this->render('admin/user-management-form.html.twig', [
          'form' => $form->createView(),
      ]);
    }

    #[Route('/user-management/delete-user/{id}', name: 'users_delete')]
    #[IsGranted("IS_AUTHENTICATED_FULLY")]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteUser(EntityManagerInterface $em, UserRepository $usersRepository, $id): Response
    {
      $user = $usersRepository->findNotAdmin($id);
      if ($user) {
        $em->remove($user);
        $em->flush();
      }
      return $this->redirectToRoute('admin_user_management');
    }

}
