<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\WorkOffer;
use App\Repository\WorkOfferRepository;
use App\Entity\WorkOfferResponse;
use App\Form\WorkOfferResponseFormType;

use App\Repository\PartnerRepository;

#[Route('/', name: 'web_routing_')]
class WebRoutingController extends AbstractController
{

    #[Route('/404', name: 'not_found')]
    public function notFound(): Response
    {
      //return $this->render('web_routing/404.html.twig');
      return new Response(
        $this->render('web_routing/404.html.twig'),
        Response::HTTP_NOT_FOUND
      );
    }

    #[Route('/', name: 'index')]
    public function index(PartnerRepository $partnerRepository): Response
    {
      $allPartners = $partnerRepository->findAll();
      return $this->render('web_routing/index.html.twig',[
        'partners' => $allPartners
      ]);
    }

    #[Route('/o-nas', name: 'about')]
    public function aboutUs(): Response
    {
        return $this->render('web_routing/about-us.html.twig');
    }

    #[Route('/informacne-systemy', name: 'information_systems')]
    public function informationSystems(): Response
    {
        return $this->render('web_routing/information-systems.html.twig');
    }

    #[Route('/pracovne-ponuky', name: 'work_offers')]
    public function workOffers(WorkOfferRepository $workOfferRepository): Response
    {
        $allOffers = $workOfferRepository->findBeforeNow();

        return $this->render('web_routing/work-offers.html.twig', [
            'allOffers' => $allOffers,
        ]);
    }

    #[Route('/pracovne-ponuky/ponuka/{slug}', name: 'work_offer_detail')]
    public function workOfferDetail(WorkOfferRepository $workOfferRepository, $slug): Response
    {
        $offer = $workOfferRepository->findOneBy(['slug' => $slug]);

        if (!$offer) {
          //vyhoď 404 error
          throw $this->createNotFoundException('Ponuka neexistuje');
        }

        $form = $this->createForm(WorkOfferResponseFormType::class, new WorkOfferResponse());

        return $this->render('web_routing/work-offer-detail.html.twig', [
            'offer' => $offer,
            'responseForm' => $form->createView(),
        ]);
    }

}
