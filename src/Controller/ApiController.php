<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use App\Entity\WorkOfferResponse;
use App\Form\WorkOfferResponseFormType;

use App\Entity\WorkOffer;
use App\Repository\WorkOfferRepository;

use Doctrine\ORM\EntityManagerInterface;
use App\Service\FileUploader;

    #[Route('/api', name: 'api_')]
class ApiController extends AbstractController
{
    #[Route('/contact-form', name: 'contact_form')]
    public function contactForm(MailerInterface $mailer, Request $request): Response
    {
      if ($request->getContent()) {
        //ak sú POST dáta, skontroluj ich
        $name = $this->cleanData($request->get("name"));
        $email = $this->cleanData($request->get("email"));
        $phone = $this->cleanData($request->get("phone"));
        $message = $this->cleanData($request->get("message"));

        //či nie sú dáta prázdne
        if (empty($name) || empty($email) || empty($phone) || empty($message)) {
          return new JsonResponse(["errMessage" => "Vyplňte všetky políčka!"]);
        }

        //email test
        if (!preg_match("/^[\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4}$/",$email)) {
          return new JsonResponse(["errMessage" => "Zadajte email v správnom formáte!"]);
        }

        //mobil test
        if (!preg_match("/^((\+4219)|(09))[0-9]{8}$/",$phone)) {
          return new JsonResponse(["errMessage" => "Zadajte telefónne číslo v správnom formáte!"]);
        }

        //odošli
        try {
          $email = (new Email())
              ->from('mailer@interconnect.com')
              ->to('contact@interconnect.com')
              ->subject('Kontakt správa')
              ->html(
                'Meno a priezvisko: '.$name.'<br>'.
                'Email: '.$email.'<br>'.
                'Telefónne číslo: '.$phone.'<br>'.
                'Správa: '.$message
              );

          $mailer->send($email);
        } catch (\Exception $e) {
          return new JsonResponse(["errMessage" => "Vašu správu sa nepodarilo odoslať."]);
        }

        return new JsonResponse(["success" => true]);
      }
    }

    #[Route('/work-offer-respond-form', name: 'work_offer_respond_form')]
    public function workOfferRespondForm(EntityManagerInterface $entityManager, WorkOfferRepository $workOfferRepository, Request $request, FileUploader $fileUploader): Response
    {
      $workOfferResponse = new WorkOfferResponse();

      $form = $this->createForm(WorkOfferResponseFormType::class, $workOfferResponse);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $workOfferResponse = $form->getData();
        $cvFile = $form->get('cvFile')->getData();

        if ($cvFile) {
            $savedCvFilename = $fileUploader->upload($cvFile);
            $workOfferResponse->setCvFile($savedCvFilename);
            //nájdi ponuku na ktorú reaguje
            $workOffer = $workOfferRepository->find($form->get('offerId')->getData());
            $workOfferResponse->setWorkOffer($workOffer);

            $entityManager->persist($workOfferResponse);
            $entityManager->flush();
        } else {
          //nebol nahratý súbor
          return new JsonResponse(["errMessage" => "Nahrajte súbor!"]);
        }

        return new JsonResponse(["success" => true]);
      }
      $errors = $form->getErrors(true, true);
      if ($errors) {
        return new JsonResponse(["errMessage" => $errors[0]->getMessage()]);
      }
      return new JsonResponse(["errMessage" => "Reakciu sa nepodarilo odoslať, skúste to neskôr."]);
    }

    function cleanData($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
}
