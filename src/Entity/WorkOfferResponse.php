<?php

namespace App\Entity;

use App\Repository\WorkOfferResponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: WorkOfferResponseRepository::class)]
class WorkOfferResponse
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: '/^[\w\-\.]+@([\w\-]+\.)+[\w\-]{2,4}$/',match: true,message: 'Zadajte email v správnom formáte!')]
    private ?string $email = null;

    #[ORM\Column(length: 255)]
    #[Assert\Regex(pattern: '/^((\+4219)|(09))[0-9]{8}$/',match: true,message: 'Zadajte telefónne číslo v správnom formáte!')]
    private ?string $phone = null;

    #[ORM\Column(length: 255)]
    private ?string $cvFile = null;

    #[ORM\ManyToOne(inversedBy: 'workOfferResponses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?WorkOffer $workOffer = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCvFile(): ?string
    {
        return $this->cvFile;
    }

    public function setCvFile(string $cvFile): static
    {
        $this->cvFile = $cvFile;

        return $this;
    }

    public function getWorkOffer(): ?WorkOffer
    {
        return $this->workOffer;
    }

    public function setWorkOffer(?WorkOffer $workOffer): static
    {
        $this->workOffer = $workOffer;

        return $this;
    }
}
