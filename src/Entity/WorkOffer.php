<?php

namespace App\Entity;

use App\Repository\WorkOfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkOfferRepository::class)]
class WorkOffer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\Column(length: 65535)]
    private ?string $description = null;

    #[ORM\Column]
    private ?int $salary = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dueDate = null;

    #[ORM\Column(length: 1000)]
    private ?string $summary = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\OneToMany(mappedBy: 'workOffer', targetEntity: WorkOfferResponse::class, orphanRemoval: true)]
    private Collection $workOfferResponses;

    public function __construct()
    {
        $this->workOfferResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): static
    {
        $this->salary = $salary;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTimeInterface $dueDate): static
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): static
    {
        $this->summary = $summary;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, WorkOfferResponse>
     */
    public function getWorkOfferResponses(): Collection
    {
        return $this->workOfferResponses;
    }

    public function addWorkOfferResponse(WorkOfferResponse $workOfferResponse): static
    {
        if (!$this->workOfferResponses->contains($workOfferResponse)) {
            $this->workOfferResponses->add($workOfferResponse);
            $workOfferResponse->setWorkOffer($this);
        }

        return $this;
    }

    public function removeWorkOfferResponse(WorkOfferResponse $workOfferResponse): static
    {
        if ($this->workOfferResponses->removeElement($workOfferResponse)) {
            // set the owning side to null (unless already changed)
            if ($workOfferResponse->getWorkOffer() === $this) {
                $workOfferResponse->setWorkOffer(null);
            }
        }

        return $this;
    }
}
