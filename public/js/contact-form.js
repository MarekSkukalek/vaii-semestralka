$(document).ready(function () {

  //po otvoreni formularu píš rovno do neho
  $('#contactModal').on('shown.bs.modal', function () {
    $('#contactName').trigger('focus');
  })

  //vlastny ajax submit
  $('#contactForm').submit(function (event) {
    event.preventDefault();
    $("#contactResponse").addClass("d-none");

    //priprav dáta
    var contactData = {
      name: $("#contactName").val(),
      email: $("#contactEmail").val(),
      phone: $("#contactPhone").val(),
      message: $("#contactMessage").val(),
    };

    var hasError = false;
    //kontrola emailu
    var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if ( !emailRegex.test(contactData.email) ) {
      //napoveda ak je neplatny
      $('#contactEmail + small').addClass('text-danger');
      $('#contactEmail  + small').removeClass('text-secondary');
      hasError = true;
    }

    //kontrola telefonneho cisla
    var phoneRegex = /^((\+4219)|(09))[0-9]{8}$/;
    if ( !phoneRegex.test(contactData.phone) ) {
      //napoveda ak je neplatny
      $('#contactPhone + small').addClass('text-danger');
      $('#contactPhone  + small').removeClass('text-secondary');
      hasError = true;
    }

    if (hasError) {
      return;
    }

    //odošli
    $.ajax({
      type: "POST",
      url: "/api/contact-form",
      data: contactData,
      dataType: "json",
      encode: true,
    }).done(function (response) {

      console.log(response);

      if (!response.success) {
        //nastala chyba na strane servera
        $("#contactResponse").text(response.errMessage);
        $("#contactResponse").removeClass("d-none");
      } else {
        //úspešné odoslanie
        $("#contactForm, #modalFooter").addClass('d-none');
        $("#contactSent").removeClass('d-none');
      }

    });

  })

  //reset upozornenia pri úprave formuláru
  $('#contactEmail, #contactPhone').on('keypress', function() {
    let contactHelpElementId = "#"+this.id+"Help";
    if ($(contactHelpElementId).hasClass('text-danger')) {
      $(contactHelpElementId).addClass('text-secondary');
      $(contactHelpElementId).removeClass('text-danger');
    }
  });

});
