$(document).ready(function () {

  //po otvoreni formularu píš rovno do neho
  $('#responseModal').on('shown.bs.modal', function () {
    $('#work_offer_response_form_name').trigger('focus');
  })

  //vlastny ajax submit
  $('#formWorkOfferResponse').submit(function (event) {
    event.preventDefault();
    $("#apiResponse").addClass("d-none");

    let emailId = "#work_offer_response_form_email";
    let phoneId = "#work_offer_response_form_phone";


    var formsData = new FormData(this);


    var hasError = false;
    //kontrola emailu
    var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if ( !emailRegex.test(formsData.get("work_offer_response_form[email]")) ) {
      //napoveda ak je neplatny
      $(emailId + ' + .help-text').addClass('text-danger');
      $(emailId + ' + .help-text').removeClass('text-secondary');
      hasError = true;
    }

    //kontrola telefonneho cisla
    var phoneRegex = /^((\+4219)|(09))[0-9]{8}$/;
    if ( !phoneRegex.test(formsData.get("work_offer_response_form[phone]")) ) {
      //napoveda ak je neplatny
      $(phoneId + ' + .help-text').addClass('text-danger');
      $(phoneId + '  + .help-text').removeClass('text-secondary');
      hasError = true;
    }

    if (hasError) {
      return;
    }

    //odošli
    $.ajax({
      type: "POST",
      url: "/api/work-offer-respond-form",
      data: formsData,
      contentType: false,
      processData:false,
      encode: true,
    }).done(function (response) {

      //console.log(response);

      if (!response.success) {
        //nastala chyba na strane servera
        $("#apiResponse").text(response.errMessage);
        $("#apiResponse").removeClass("d-none");
      } else {
        //úspešné odoslanie
        $("#formBody, #modalFooter").addClass('d-none');
        $("#formSent").removeClass('d-none');
      }

    });

  })


  //reset upozornenia pri úprave formuláru
  $('#work_offer_response_form_email, #work_offer_response_form_phone').on('keypress', function() {
    let contactHelpElementId = "#"+this.id+" + .help-text";
    if ($(contactHelpElementId).hasClass('text-danger')) {
      $(contactHelpElementId).addClass('text-secondary');
      $(contactHelpElementId).removeClass('text-danger');
    }
  });

});
