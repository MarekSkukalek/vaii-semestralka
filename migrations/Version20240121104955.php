<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240121104955 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE work_offer_response (id INT AUTO_INCREMENT NOT NULL, work_offer_id INT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, cv_file VARCHAR(255) NOT NULL, INDEX IDX_66F292558673FEB2 (work_offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE work_offer_response ADD CONSTRAINT FK_66F292558673FEB2 FOREIGN KEY (work_offer_id) REFERENCES work_offer (id)');
        $this->addSql('ALTER TABLE work_offer CHANGE description description VARCHAR(65535) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE work_offer_response DROP FOREIGN KEY FK_66F292558673FEB2');
        $this->addSql('DROP TABLE work_offer_response');
        $this->addSql('ALTER TABLE work_offer CHANGE description description MEDIUMTEXT NOT NULL');
    }
}
